# 硬體
### 點擊Menu進入選單
![](picture/001.jpg)


------------

### 選擇Output
![](picture/002.jpg)
------------

### 選擇Mode
![](picture/003.jpg)
------------

### 設定模式為Curve後 按Back回上一頁
![](picture/004.jpg)
------------

### 選擇SAS
![](picture/005.jpg)
------------

### 選擇Curve
![](picture/006.jpg)
------------

### 設定各參數後 選擇[Set Curve]按確定
![](picture/007.jpg)
